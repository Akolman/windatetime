//! A timestamp library which uses the Windows DateTime origin (Jan, 01 0001).
//!
//! Provides platform-agnostic handling of DateTime values, with methods to convert to/from DateTime, FILETIME, UNIX Epoch Time, and Rust SystemTime.
//!
//! The implementation of the tick->date conversion is based off of the .netcore DateTime library.  Outside of a few extensions such as the UNIX Epoch handling, WinDateTime should provide identical values to that of .netcore DateTime.
//!
//! WinDateTime is not intended to perform filesystem changes and therefor doesn't have methods that allow to alter the timestamps of files.
//! There's nothing special about this library; the only slightly complex logic is that which parses a tick value to a date.  It's primary use is to convert from timestamps that exist within the progrm versus those pulled directly from a filesystem.
//!
//! The only value stored internally is the tick value.  All functions and methods will calculated date/time values when called.  This obviously saves time instantiating a WinDateTime,
//! with a cost incurred when fetching the date/time attributes.  For that reason, the intention is to use a single function or method call to get the necessary value.
//!
//! # Purpose:
//! Other Rust date/time libraries are based solely off of the UNIX Epoch time (1970-01-01).
//! This is unhelpful when dealing with values that use other timestamp formats such as FILETIME, where the epoch is 1601-01-01.  While [filetime](https://docs.rs/filetime)
//! is capable of handling FILETIME values, it is intended for use with filesystems.
//!
//! WinDateTime seeks to simplify conversion to and from these formats by providng a structure based off of Window's DateTime format.  From/as functions are provided to handle conversion
//! from/to UNIX Epoch time, FILETIME and Rust's SystemTime (from only).

use std::fmt;
use std::time::SystemTime;

/// An instant in item, represented as a date and time, from the origin 01/01/0001 at 00:00:00.0000.
///
/// A basic implementation of the [Windows DateTime struct](https://docs.microsoft.com/en-us/dotnet/api/system.datetime?view=netcore-3.1)
/// with extensions to convert to/from Linux Epoch time, and Windows FILETIME time.
pub struct WinDateTime {
    _ticks: i64,
}

/// The earliest time that can be represented
const ORIGIN: i64 = MIN_TICKS;
/// The latest time that can be represented
const END: i64 = MAX_TICKS;

/// The number of ticks per millisecond (1 tick == 100 nanoseconds)
const TICKS_PER_MILLISECOND: i64 = 10000;
/// The number of ticks per second (10,000,000)
const TICKS_PER_SECOND: i64 = TICKS_PER_MILLISECOND * 1000;
/// The number of ticks per minute (600,000,000)
const TICKS_PER_MINUTE: i64 = TICKS_PER_SECOND * 60;
/// The number of ticks per hour (36,000,000,000)
const TICKS_PER_HOUR: i64 = TICKS_PER_MINUTE * 60;
/// The number of ticks per day (864,000,000,000)
const TICKS_PER_DAY: i64 = TICKS_PER_HOUR * 24;

/// Day of year, by month, for non-leap years
const DAYS_TO_MONTH_365: [i32; 13] = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365];
/// Day of year, by month, for leap years
const DAYS_TO_MONTH_366: [i32; 13] = [0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366];

const DAYS_PER_YEAR: i32 = 365;
/// Number of days in 4 years (1461)
const DAYS_PER_4_YEARS: i32 = DAYS_PER_YEAR * 4 + 1;
/// Number of days in 100 years (36,524)
const DAYS_PER_100_YEARS: i32 = DAYS_PER_4_YEARS * 25 - 1;
/// Number of days in 400 years (146,097)
const DAYS_PER_400_YEARS: i32 = DAYS_PER_100_YEARS * 4 + 1;

/// Number of days to 10000 years (3,652,059)
const DAYS_TO_10000: i64 = (DAYS_PER_400_YEARS * 25 - 366) as i64;

/// The minimum number of ticks that can be understood
const MIN_TICKS: i64 = 0;
/// The maximum number of ticks that can be understood
const MAX_TICKS: i64 = DAYS_TO_10000 * TICKS_PER_DAY - 1;

/// Offset between DateTime origin and FileTime origin, in ticks
const FILETIME_ORIGN_DIFF: i64 = 504911232000000000;
/// Offset between DateTime origin and Linux Epoch, in ticks
const EPOCH_ORIGN_DIFF: i64 = 621355968000000000;

const YEAR_DATE_PART: i32 = 1;
const MONTH_DATE_PART: i32 = 2;
const DAY_DATE_PART: i32 = 3;
const HOUR_DATE_PART: i32 = 4;
const MINUTE_DATE_PART: i32 = 5;
const SECOND_DATE_PART: i32 = 6;
const MILLISECOND_DATE_PART: i32 = 7;

impl WinDateTime {
    /// Creates a new WinDateTime instance using the number of 100-nanosecond intervals from Jan 1, 0001 at 00:00:00.000
    pub fn new(ticks: i64) -> Self {
        if ticks < MIN_TICKS || ticks > MAX_TICKS {
            panic!("tick value out of range");
        }
        return Self { _ticks: ticks };
    }

    /// Parses an ISO8601 format date string and returns a WinDateTime instance.  String parsing is strict and expects a value with format:
    /// `YYYY-MM-DD HH:MM:SS.MMM`
    /// Timesonze currently optional but UTC is assumed either way.
    pub fn parse(date_string: String) -> Self {
        let year = date_string[0..4].parse::<i32>().unwrap();
        let month = date_string[5..7].parse::<i32>().unwrap();
        let day = date_string[8..10].parse::<i32>().unwrap();
        let hour = date_string[11..13].parse::<i32>().unwrap();
        let minute = date_string[14..16].parse::<i32>().unwrap();
        let second = date_string[17..19].parse::<i32>().unwrap();

        return WinDateTime::from_date_time(year, month, day, hour, minute, second, 0);
    }

    /// Creates a new WinDateTime instance using year, month, and day values.
    pub fn from_date(year: i32, month: i32, day: i32) -> Self {
        return WinDateTime::new(WinDateTime::date_to_ticks(year, month, day));
    }

    /// Creates a new WinDateTime instance using all attributes of a date/time instant.
    pub fn from_date_time(
        year: i32,
        month: i32,
        day: i32,
        hour: i32,
        minute: i32,
        second: i32,
        millisecond: i32,
    ) -> Self {
        return WinDateTime::new(
            WinDateTime::date_to_ticks(year, month, day)
                + WinDateTime::time_to_ticks(hour, minute, second, millisecond),
        );
    }

    /// Creates a new WinDateTime instance using a FILETIME value in ticks.  That is, the number of ticks from 01/01/1601.
    pub fn from_filetime(time: i64) -> Self {
        return WinDateTime::new(time + FILETIME_ORIGN_DIFF);
    }

    /// Creates a new WinDateTime instance using a UNIX Epoch time value in *milliseconds*.  This is, the number of ticks from 01/01/1970.
    ///
    /// Note that UNIX Epoch values are never given in sub-millisecond resolution, so this function calls for a value in milliseconds.
    pub fn from_unix_epoch(time: i64) -> Self {
        return WinDateTime::new(time * TICKS_PER_MILLISECOND + EPOCH_ORIGN_DIFF);
    }

    /// Creates a new WinDateTime instance using a Rust [SystemTime](https://doc.rust-lang.org/std/time/struct.SystemTime.html) instance.
    pub fn from_system_time(system_time: SystemTime) -> Self {
        return WinDateTime::from_unix_epoch(
            system_time
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_millis() as i64,
        );
    }

    /// Gets the current date and time in the form of a WinDateTime instance.
    pub fn now() -> Self {
        return WinDateTime::from_system_time(SystemTime::now());
    }

    /// Gets the origin WinDateTime (01/01/0001).
    pub fn origin() -> Self {
        return WinDateTime::new(ORIGIN);
    }

    /// Gets the latest WinDateTime (12/31/9999).
    pub fn end() -> Self {
        return WinDateTime::new(END);
    }

    /// Const for getting the Linux Epoch (01/01/1970) as a WinDateTime
    #[allow(non_snake_case)]
    pub fn UNIX_EPOCH() -> Self {
        return WinDateTime::from_unix_epoch(0);
    }

    /// Returns the value of the WinDateTime as a FILETIME
    pub fn as_filetime(&self) -> i64 {
        return self.get_value() - FILETIME_ORIGN_DIFF;
    }

    /// Returns the value of the WinDateTime as a Linux Epoch timestamp.
    ///
    /// As with the from_unix_epoch, since UNIX Epoch values are usually given in milliseconds, this call returns the value in milliseconds.
    pub fn as_unix_epoch(&self) -> i64 {
        return (self.get_value() - EPOCH_ORIGN_DIFF) / TICKS_PER_MILLISECOND;
    }

    /*
    pub fn add(&self, other: WinDateTime) -> Self {
        return WinDateTime::new(self._ticks + other._ticks);
    }*/

    /// Gets the 'ticks' value of the current WinDateTime instance.
    pub fn get_value(&self) -> i64 {
        return self._ticks;
    }

    /// Gets the year value of the current WinDateTime instance.
    pub fn year(&self) -> i32 {
        return self.get_date_part(YEAR_DATE_PART);
    }

    /// Gets the month value of the current WinDateTime instance.
    pub fn month(&self) -> i32 {
        return self.get_date_part(MONTH_DATE_PART);
    }

    /// Gets the day value of the current WinDateTime instance.
    pub fn day(&self) -> i32 {
        return self.get_date_part(DAY_DATE_PART);
    }

    /// Gets the hour value of the current WinDateTime instance.
    pub fn hour(&self) -> i32 {
        return self.get_date_part(HOUR_DATE_PART);
    }

    /// Gets the minute value of the current WinDateTime instance.
    pub fn minute(&self) -> i32 {
        return self.get_date_part(MINUTE_DATE_PART);
    }

    /// Gets the second value of the current WinDateTime instance.
    pub fn second(&self) -> i32 {
        return self.get_date_part(SECOND_DATE_PART);
    }

    /// Gets the millisecond value of the current WinDateTime instance.
    pub fn millisecond(&self) -> i32 {
        return self.get_date_part(MILLISECOND_DATE_PART);
    }

    pub fn to_iso_string(&self) -> String {
        let (
            year_part,
            month_part,
            day_part,
            hour_part,
            minute_part,
            second_part,
            millisecond_part,
        ) = self.get_date_parts();
        return String::from(format!(
            "{:0>4}-{:0>2}-{:0>2}T{:0>2}:{:0>2}:{:0>2}.{:0>3}Z",
            year_part, month_part, day_part, hour_part, minute_part, second_part, millisecond_part
        ));
    }

    pub fn get_date_part(&self, date_part: i32) -> i32 {
        let ticks = self._ticks;

        //number of days since origin
        let mut n = (ticks / TICKS_PER_DAY) as i32;

        //y400 = number of whole 400-year periods since origin
        let y400 = n / DAYS_PER_400_YEARS;

        //n = day number within 400-year period
        n = n - y400 * DAYS_PER_400_YEARS;

        //y100 = number of whole 100-year periods within 400-year period
        let mut y100 = n / DAYS_PER_100_YEARS;

        //Last 100-year period has an extra day, so decrement result if 4
        if y100 == 4 {
            y100 = 3;
        }

        //n = day number within 100-year period
        n = n - y100 * DAYS_PER_100_YEARS;

        //y4 = number of whole 4-year periods within 100-year period
        let y4 = n / DAYS_PER_4_YEARS;

        //n = day number within 4-year period
        n = n - y4 * DAYS_PER_4_YEARS;

        //y1 = number of whole years within 4-year period
        let mut y1 = n / DAYS_PER_YEAR;

        //Last year has an extra day, so decrement result if 4
        if y1 == 4 {
            y1 = 3;
        }

        let year = y400 * 400 + y100 * 100 + y4 * 4 + y1 + 1;
        if date_part == YEAR_DATE_PART {
            return year;
        }

        //n = day number within year
        n = n - y1 * DAYS_PER_YEAR;

        let mut is_leap_year = false;
        if y1 == 3 && (y4 != 24 || y100 == 3) {
            is_leap_year = true;
        }

        let mut days = DAYS_TO_MONTH_365;
        if is_leap_year {
            days = DAYS_TO_MONTH_366;
        }

        let mut m = (n >> 5) + 1;

        while n >= days[m as usize] {
            m = m + 1;
        }

        let month = m;

        if date_part == MONTH_DATE_PART {
            return month;
        }

        let day = n - days[(m - 1) as usize] + 1;

        if date_part == DAY_DATE_PART {
            return day;
        } else if date_part == HOUR_DATE_PART {
            return ((ticks / TICKS_PER_HOUR) % 24) as i32;
        } else if date_part == MINUTE_DATE_PART {
            return ((ticks / TICKS_PER_MINUTE) % 60) as i32;
        } else if date_part == SECOND_DATE_PART {
            return ((ticks / TICKS_PER_SECOND) % 60) as i32;
        } else {
            return ((ticks / TICKS_PER_MILLISECOND) % 1000) as i32;
        }
    }

    pub fn get_date_parts(&self) -> (i32, i32, i32, i32, i32, i32, i32) {
        let ticks = self._ticks;

        let mut n = (ticks / TICKS_PER_DAY) as i32;

        let y400 = n / DAYS_PER_400_YEARS;

        n = n - y400 * DAYS_PER_400_YEARS;

        let mut y100 = n / DAYS_PER_100_YEARS;

        if y100 == 4 {
            y100 = 3;
        }

        n = n - y100 * DAYS_PER_100_YEARS;

        let y4 = n / DAYS_PER_4_YEARS;

        n = n - y4 * DAYS_PER_4_YEARS;

        let mut y1 = n / DAYS_PER_YEAR;

        if y1 == 4 {
            y1 = 3;
        }

        let year = y400 * 400 + y100 * 100 + y4 * 4 + y1 + 1;

        n = n - y1 * DAYS_PER_YEAR;

        let mut is_leap_year = false;
        if y1 == 3 && (y4 != 24 || y100 == 3) {
            is_leap_year = true;
        }

        let mut days = DAYS_TO_MONTH_365;
        if is_leap_year {
            days = DAYS_TO_MONTH_366;
        }

        let mut m = (n >> 5) + 1;

        while n >= days[m as usize] {
            m = m + 1;
        }

        let month = m;

        let day = n - days[(m - 1) as usize] + 1;

        let hour = (ticks / TICKS_PER_HOUR) % 24;

        let minute = (ticks / TICKS_PER_MINUTE) % 60;

        let second = (ticks / TICKS_PER_SECOND) % 60;

        let millisecond = (ticks / TICKS_PER_MILLISECOND) % 1000;

        return (
            year,
            month,
            day,
            hour as i32,
            minute as i32,
            second as i32,
            millisecond as i32,
        );
    }

    fn date_to_ticks(year: i32, month: i32, day: i32) -> i64 {
        let days: [i32; 13];
        if WinDateTime::is_leap_year(year) {
            days = DAYS_TO_MONTH_366;
        } else {
            days = DAYS_TO_MONTH_365;
        }

        let y = year - 1;
        let n = y * 365 + y / 4 - y / 100 + y / 400 + days[(month - 1) as usize] + day - 1;

        return n as i64 * TICKS_PER_DAY;
    }

    fn time_to_ticks(hour: i32, minute: i32, second: i32, millisecond: i32) -> i64 {
        return ((hour as i64) * TICKS_PER_HOUR
            + (minute as i64) * TICKS_PER_MINUTE
            + (second as i64) * TICKS_PER_SECOND
            + (millisecond as i64) * TICKS_PER_MILLISECOND) as i64;
    }

    fn is_leap_year(year: i32) -> bool {
        if year < 1 || year > 9999 {
            panic!("Invalid year value");
        }

        return (year & 3) == 0 && ((year & 15) == 0 || (year % 25) != 0);
    }
}

impl fmt::Display for WinDateTime {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let (
            year_part,
            month_part,
            day_part,
            hour_part,
            minute_part,
            second_part,
            millisecond_part,
        ) = self.get_date_parts();
        write!(
            f,
            "{:0>4}-{:0>2}-{:0>2} {:0>2}:{:0>2}:{:0>2}.{:0>3}",
            year_part, month_part, day_part, hour_part, minute_part, second_part, millisecond_part,
        )
    }
}
