extern crate windatetime;
use std::fs::File;

#[test]
#[should_panic(expected = "tick value out of range")]
fn test_min_exceed() {
    let _ = windatetime::WinDateTime::new(-1);
}

#[test]
#[should_panic(expected = "tick value out of range")]
fn test_max_exceed() {
    let _ = windatetime::WinDateTime::new(3155378976000000000);
}

#[test]
fn test_new() {
    let some_time = windatetime::WinDateTime::new(637294617301230000);
    assert_eq!(some_time.get_date_parts(), (2020, 7, 4, 12, 15, 30, 123));
}

#[test]
fn test_epoch() {
    let epoch_origin = windatetime::WinDateTime::UNIX_EPOCH();
    let parsed_epoch = windatetime::WinDateTime::parse(String::from("1970-01-01T00:00:00Z"));
    assert_eq!(epoch_origin.get_value(), parsed_epoch.get_value());
}

#[test]
fn test_rust_systemtime() {
    //600,000,000
    let _ = std::fs::remove_file("fileintotest.txt");
    let file = File::create("fileintotest.txt").unwrap();

    let metadata = file.metadata().unwrap();
    let mod_systemtime = metadata.modified().unwrap();
    let windatetime_modtime = windatetime::WinDateTime::from_system_time(mod_systemtime);
    let windatetime_now = windatetime::WinDateTime::now();

    let diff = windatetime_now.get_value() - windatetime_modtime.get_value();

    let _ = std::fs::remove_file("fileintotest.txt").unwrap();

    assert!(diff < 100000000);
}

#[test]
fn test_to_string() {
    let some_day = windatetime::WinDateTime::new(626591822100000000);

    assert_eq!(some_day.to_iso_string(), "1986-08-05T00:23:30.000Z");
}
