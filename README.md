# windatetime

A timestamp library which uses the Windows DateTime origin (Jan, 01 0001).

Provides platform-agnostic handling of DateTime values, with methods to convert to/from DateTime, FILETIME, UNIX Epoch Time, and Rust SystemTime.

The implementation of the tick->date conversion is based off of the .netcore DateTime library. Outside of a few extensions such as the UNIX Epoch handling, WinDateTime should provide identical values to that of .netcore DateTime.

WinDateTime is not intended to perform filesystem changes and therefore doesn't have methods that allow to alter the timestamps of files.
There's nothing special about this library; the only slightly complex logic is that which parses a tick value to a date. It's primary use is to convert from timestamps that exist within memory versus those pulled directly from a filesystem.

The only value stored internally is the tick value. All functions and methods will calculated date/time values when called. This obviously saves time instantiating a WinDateTime, with a cost incurred when fetching the date/time attributes. For that reason, the intention is to use a single function or method call to get the necessary value.

# Purpose:

Other Rust date/time libraries are based solely off of the UNIX Epoch time (1970-01-01).
This is unhelpful when dealing with values that use other timestamp formats such as FILETIME, where the epoch is 1601-01-01. While [filetime](https://docs.rs/filetime)
is capable of handling FILETIME values, it is intended for use with filesystems.

WinDateTime seeks to simplify conversion to and from these formats by providng a structure based off of Window's DateTime format. From/as functions are provided to handle conversion from/to UNIX Epoch time, FILETIME and Rust's SystemTime (from only).
